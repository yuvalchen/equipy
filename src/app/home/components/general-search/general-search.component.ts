import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-general-search',
  templateUrl: './general-search.component.html',
  styleUrls: ['./general-search.component.sass']
})
export class GeneralSearchComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit() {
  }

}
